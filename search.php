<?php
    require_once 'header.php';
    require_once 'inc/manager-db.php';
?>

<!-- Cette page est le tableau du résultat de la recherche effectuée -->
<div class="ui container">
    <?php
        if (isset($_SESSION['login']) && isset($_SESSION['password']) && isset($_SESSION['role'])):
            $lesPays = getSearch($_GET);
        ?>
        <h1 class="ui center aligned header"> <i> <u> Recherche </u> </i></h1>
        <br>
        <table class = "ui selectable celled table">
            <thead>
                <tr class="center aligned">
                    <th>  Nom </th>
                    <th> Code </th>
                    <th> Région </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($lesPays as $value): ?>
                    <tr>
                        <td>
                            <?php 
                                $code = strtolower($value->Code2);
                                $nomPays = $value->Name; 
                            ?>
                            <i class="<?php echo $code; ?> flag"></i>

                            <a class="nav-link" href="informationsPays.php?nom=<?php echo $nomPays;?>"> <?php  echo $nomPays; ?>
                        </td>
                        <td> <?php  echo $value->Code; ?></td>
                        <td> <?php  echo $value->Region; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <br>

    <?php endif; ?>
</div>

<?php
    require_once 'javascripts.php';
    require_once 'footer.php';
?>