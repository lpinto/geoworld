<?php
    require_once 'header.php';
    require_once 'inc/manager-db.php';
?>

<div class="ui container">
    <?php
        if (isset($_SESSION['login']) && isset($_SESSION['password']) && isset($_SESSION['role'])):

            /** Si l'utilisateur est l'administrateur et qu'il n'y a pas d'identifiant dans l'URL
             * on affiche la liste des utilisateurs inscrit
             */
            if ($_SESSION['role'] == "admin" && !isset($_GET['id'])):
                $users = getAllUsers();
            ?>

            <h1 class="ui center aligned header"> <i> <u> Les utilisateurs </u> </i></h1>
            <br>
            <table class = "ui selectable celled table">
                <thead>
                <tr class="center aligned">
                    <th>  Identifiant </th>
                    <th> Nom </th>
                    <th> Prénom </th>
                    <th> Login </th>
                    <th> Password </th>
                    <th> Rôle </th>
                    <th> Delete </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $value): ?>
                    <tr class="center aligned">
                        <td> <?php echo $value->id ?></td>
                        <td> <a href="index.php?id=<?php echo $value->id ?>">  <?php  echo $value->nom; ?> </a></td>
                        <td> <?php  echo $value->prenom; ?></td>
                        <td> <?php  echo $value->login; ?></td>
                        <td> <?php  echo $value->password; ?></td>
                        <td> <?php  echo $value->role; ?></td>
                        <td> <a class="nav-link" href = "delete.php?id=<?php echo $value->id ?>" onclick ="return confirm('Êtes-vous sûr de supprimer <?php echo $value->login ?>?')"> delete </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <br>

            <p> <a href="inscription.php" >Ajout d'un utilisateur </a></p>

            <?php endif;

            /** Si l'utilisateur est l'enseignant ou que c'est l'administrateur et qu'il y a un identifiant
             * on affiche la liste des requêtes de l'enseignant
             */
            if ($_SESSION['role'] == "enseignant" || ($_SESSION['role'] == "admin" && isset($_GET['id']))):
                if ($_SESSION['role'] == "admin"){
                    $requetes = getAllRequetes($_GET['id']);
                }
                else{
                    $requetes = getAllRequetes($_SESSION['id']);
                }
            ?>
                <h1 class="ui center aligned header"> <i> <u> Les requêtes </u> </i></h1>
                <br>
                <?php if (count($requetes) != 0) : ?>
                    <table class="ui selectable celled table">
                        <thead>
                        <tr class="center aligned">
                            <th> Nom </th>
                            <th> Requête </th>
                            <th> Type </th>
                            <th> Delete </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($requetes as $value): ?>
                            <tr class="center aligned">
                                <td> <?php  echo $value->nomSQL; ?></td>
                                <td>
                                    <form action = "afficheSQL.php" method="POST">
                                        <button class="link_but" type="submit" name = "requete" value = '<?php  echo $value->requete; ?>'> <?php  echo $value->requete; ?> </button>
                                    </form>
                                </td>
                                <td> <?php echo $value->type; ?></td>
                                <td> <a class="nav-link" href = "delete.php?idSQL=<?php echo $value->idSQL ?>&idUser=<?php echo $_SESSION['id']; ?>" onclick ="return confirm('Êtes-vous sûr de supprimer ?')"> Delete </td>

                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif;
                if (count($requetes) == 0): ?>
                <h3 class="ui center aligned header"> Aucune Requête </h3>
                <?php endif; ?>

            <br>
                <?php endif; ?>
            <!-- Si c'est un élève alors on affiche toute les requêtes publique des enseignants-->
            <?php if ($_SESSION['role'] == "eleve"):
                $requetes = getAllRequetesPub();
            ?>
                <h1 class="ui center aligned header"> <i> <u> Les requêtes </u> </i></h1>
                <br>
                <table class="ui selectable celled table">
                    <thead>
                    <tr class="center aligned">
                        <th> Nom </th>
                        <th> Requête </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($requetes as $value): ?>
                        <tr class="center aligned">
                            <td> <?php  echo $value->nomSQL; ?></td>
                            <td>
                                <form action = "afficheSQL.php" method="POST">
                                    <button class="link_but" type="submit" name = "requete" value = '<?php  echo $value->requete; ?>'> <?php  echo $value->requete; ?> </button>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <br>
            <?php endif; ?>
        <?php endif;
        /** Si aucune session est ouverte on se redirige vers la page de connexion */
        if (!isset($_SESSION['login']) && !isset($_SESSION['password']) && !isset($_SESSION['role'])){
            header('location:login.php');
            exit();
        } ?>

</div>

<?php
    require_once 'javascripts.php';
    require_once 'footer.php';
?>
