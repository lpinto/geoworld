<?php require_once 'header.php';?>

<!-- Cette page est un formulaire d'inscription pour ajouter un enseignant -->

<div class="ui container">
	<div class="page-login">
	 	<div class="ui centered grid container">
		  	<div class="nine wide column">
		      	<h2>
		      		<div >
			        	<h2 class="ui center aligned header"> Création du compte</h2>
			      	</div>
		      	</h2>	
		  
		      	<div class="ui fluid card">
		      		<div class="content">
			      		<form action="ajoutBase.php" method="post" class="ui form"> 
						    <div class="field">
                                Civilité :
                                <div class="ui radio checkbox">
                                    <input type="radio"  name="civilite" value="madame" />
                                    <label> Madame </label>
                                </div>

                                <div class="ui radio checkbox">
                                    <input type="radio" name="civilite" value ="monsieur"/>
                                    <label> Monsieur </label>
                                </div>
                            </div>
							<div class="two fields">
								<div class="field">
									<label> Nom </label>
									<input placeholder="Nom" name="nom" type="text">
								</div>

								<div class="field">
									<label> Prénom </label>
									<input placeholder="Prénom" name="prenom" type="text">
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label> Login </label>
									<input placeholder="Login" name="login" type="text">
								</div>

								<div class="field">
									<label> Mot de passe </label>
									<input type="password" name="password" placeholder="password">
								</div>
							</div>
							<button class="ui right floated button">Valider</button>
				       </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php
    require_once 'javascripts.php';
    require_once 'footer.php';
?>