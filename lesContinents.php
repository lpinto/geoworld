<?php  
    require_once 'header.php';
    require_once 'inc/manager-db.php';
    $continent = $_GET['continent'];
    $lesPays = getCountriesByContinent($continent, $_GET);
    $nbPage = nbPage($continent);
?>

<!-- Cette page affiche dans un tableau de plusieurs page les pays du continent choisi -->
<div class="ui container">
    <?php if (isset($_SESSION['login']) && isset($_SESSION['password']) && isset($_SESSION['role'])): ?>
        <div>
            <h1 class="ui center aligned header"> <i> <u> Les pays en <?php echo $continent; ?></u></i> </h1>
            <br>
                <table class = "ui celled table">
                    <thead>
                        <tr class="center aligned">
                            <th>  Nom </th>
                            <th> Code </th>
                            <th> Région </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($lesPays as $value): ?>
                            <tr>
                                <td>
                                    <?php
                                        $code = strtolower($value->Code2);
                                        $nomPays = $value->Name;
                                    ?>
                                    <i class="<?php echo $code; ?> flag"></i>

                                    <a class="nav-link" href="informationsPays.php?nom=<?php echo $nomPays;?>"> <?php  echo $nomPays; ?> </a>
                                </td>
                                <td> <?php  echo $value->Code; ?></td>
                                <td> <?php  echo $value->Region; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <br>

        </div>
        <div class="ui pagination menu">
            <?php
                for($i = 1; $i <= $nbPage; $i++):
                    if($i == $_GET['page']):
            ?>
                        <a class="active item" href="lesContinents.php?continent=<?php echo $continent;?>&page=<?php echo $i; ?>"><?php echo $i; ?></a>
                    <?php endif;
                        if($i != $_GET['page']):
                    ?>
                        <a class="item" href="lesContinents.php?continent=<?php echo $continent;?>&page=<?php echo $i; ?>"><?php echo $i; ?></a>
                    <?php endif; ?>
            <?php endfor; ?>
        </div>
    <?php endif; ?>
</div>



<?php
    require_once 'javascripts.php';
    require_once 'footer.php';
?>
