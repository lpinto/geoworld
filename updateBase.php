<?php
    require_once 'inc/manager-db.php';

    /** On modifie les informations du pays */
    if (isset($_POST['idCountry'])){
        updateCountry($_POST);
        header("location:informationsPays.php?nom=".$_POST["NameCountry"]."");
        exit();
    }
    /** On modifie les langues du pays */
    else if (isset($_POST['idLanguage'])){
        updateLanguage($_POST);
        header("location:informationsPays.php?nom=".$_POST["NameCountry"]."");
        exit();
    }
?>