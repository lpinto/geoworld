<?php 
	require_once 'header.php';
	require_once 'inc/manager-db.php';

	//on teste si nos variables sont définies et remplies
	if (isset($_POST['login']) && isset($_POST['password']) && !empty($_POST['login']) && !empty($_POST['password'])){

		$result = getAuthentification($_POST['login'], $_POST['password']);

		if($result){
    		// on la démarre la session
    		session_start (); 
    		// on enregistre les paramètres de notre visiteur comme variables de session
            $_SESSION['id'] = $result->id;
            $_SESSION['civilite'] = $result->civilite;
    		$_SESSION['login'] = $result->login; 
    		$_SESSION['password'] = $result->password;
    		$_SESSION['role'] = $result->role;

       		header('location:index.php');
       		exit();
    	}
    	else{
    		header('location:erreurLogin.php');
    		exit();
    	}
    }
    else{
    		header('location:erreurLogin.php');
            exit();
    	}

?>