<?php
    require_once 'inc/manager-db.php';

    /** Si id existe alors on supprime l'utilisateur à la base
     */
	if (isset($_GET['id'])){
		deleteUser($_GET['id']);
	}

    /** Sinon if requete existe on supprime la requête à la base */
	else if (isset($_GET['idSQL'])){
		DeleteRequete($_GET['idSQL'], $_GET['idUser']);
	}

    /** On se redirige vers la page d'accueil */
    header('location:index.php');
    exit();
?>