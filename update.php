<?php
require_once 'header.php';
require_once 'inc/manager-db.php';
$lePays = $_GET['nom'];
$geop = geopolitique($lePays);
$officielle = getOfficialLanguage($lePays);
$nonOfficielle = getUnofficialLanguage($lePays);
$infoCapital = capital($lePays);
?>
<!-- Cette page est utilisé pour la modification des données du pays-->
<div class="ui container">
    <form action="updateBase.php" method="post">
        <?php foreach ($geop as $value) {
            $code = strtolower($value->Code2);
        }
        ?>

        <h1><img class="ui avatar" src="images/drapeau/<?php echo $code; ?>.png" alt="drapeau">
            <input type="text" name="NameCountry" value= "<?php echo $value->Name;?>" >  </h1>

        <div class="ui two column grid">
            <div class="column">
                <div class="ui raised segment">
                    <a class="ui red ribbon label"> <h4> Géopolitique </h4>  </a>
                    <?php foreach ($geop as $value): ?>
                        <ul class="ui list">
                            <input type="hidden" name="idCountry" value="<?php echo $value->id; ?>">
                            <li> <h4>Population : <input type="text" name="PopulationCountry" value= "<?php echo $value->Population; ?>" > </h4> </li>
                            <li> <h4> Espérance de vie : <input type="text" name="LifeExpectancy" value="<?php echo $value->LifeExpectancy; ?>" > </h4> </li>
                            <li> <h4> Superficie : <input type="text" name="SurfaceArea" value="<?php echo $value->SurfaceArea; ?>" ></h4> </li>
                            <li> <h4> L'année de l'indépendance : <input type="text" name="IndepYear" value="<?php echo $value->IndepYear; ?>" ></h4> </li>
                            <li> <h4> Type de gouvernement : <input type="text" name="GovernmentForm" value="<?php echo $value->GovernmentForm; ?>" ></h4></li>
                            <li> <h4> Chef de l'Etat : <input type="text" name="HeadOfState" value="<?php echo $value->HeadOfState; ?>" > </h4> </li>
                        </ul>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="column">
                <div class="ui raised segment">
                    <a class="ui teal right ribbon label"> <h4> Capital </h4> </a>
                    <?php if (sizeof($infoCapital) != 0): ?>
                        <?php foreach ($infoCapital as $value): ?>
                            <ul class="ui list">
                                <input type="hidden" name="idCity" value="<?php echo $value->id; ?>">
                                <li> <h4>Nom : <input type="text" name="NameCity" value="<?php echo $value->Name; ?>" > </h4> </li>
                                <li> <h4> Population : <input type="text" name="PopulationCity" value="<?php echo $value->Population; ?>" > </h4> </li>
                            </ul>
                        <?php endforeach; ?>
                    <?php endif;?>
                    <?php if (sizeof($infoCapital) == 0): ?>
                        Pas de capital
                    <?php endif;?>
                </div>
            </div>
            <div>
                <button class="ui inverted green button">Valider</button>
                <a href="informationsPays.php?nom=<?php echo $lePays;?>" >
                    <input type="button" value="Annuler" class="ui inverted red button" onClick="return(confirm('Etes-vous sûr de vouloir annuler ?'));">
                </a>
            </div>
        </div>
    </form>
    <br><br>

    <div class="ui one column grid">
        <div class="column">
            <div class="ui raised segment">
                <a class="ui blue ribbon label">  <h4> Langues </h4> </a>

                <?php if (sizeof($officielle) != 0):; ?>
                    <h4>Officielle :</h4>
                    <?php foreach ($officielle as $value):?>
                        <form action="updateBase.php" method="post">
                            <ul class="ui list">
                                <li>
                                    <input type="hidden" name="NameCountry" value="<?php echo $lePays; ?>">
                                    <input type="hidden" name="idLanguage" value="<?php echo $value->idLanguage; ?>">
                                    <input type="hidden" name="IsOfficial" value="<?php echo $value->IsOfficial; ?>">
                                    <input type="text" name="NameLanguage" value="<?php echo $value->Name;?>" size = 13> parlé par
                                    <input class="percentage" type="text" name="Percentage" value="<?php echo $value->Percentage;?>" size = 1> % de la population
                                    <button class="ui inverted green button">Valider</button>
                                    <a href="informationsPays.php?nom=<?php echo $lePays;?>" >
                                        <input type="button" value="Annuler" class="ui inverted red button" onClick="return(confirm('Etes-vous sûr de vouloir annuler ?'));">
                                    </a>
                                </li>
                            </ul>
                        </form>
                    <?php endforeach ;?>
                <?php endif; ?>

                <?php if (sizeof($nonOfficielle) != 0):; ?>
                    <h4>Non Officielle :</h4>
                    <?php foreach ($nonOfficielle as $value):?>
                        <form action="updateBase.php" method="post">
                            <ul class="ui list">
                                <li>
                                    <input type="hidden" name="NameCountry" value="<?php echo $lePays; ?>">
                                    <input type="hidden" name="idLanguage" value="<?php echo $value->idLanguage; ?>">
                                    <input type="hidden" name="IsOfficial" value="<?php echo $value->IsOfficial; ?>">
                                    <input type="text" name="NameLanguage" value= "<?php echo $value->Name;?>" size = 13> parlé par
                                    <input class="percentage" type="text" name="Percentage" value= "<?php echo $value->Percentage;?>" size = 1> % de la population
                                    <button class="ui inverted green button">Valider</button>
                                    <a href="informationsPays.php?nom=<?php echo $lePays;?>" >
                                        <input type="button" value="Annuler" class="ui inverted red button" onClick="return(confirm('Etes-vous sûr de vouloir annuler ?'));">
                                    </a>
                                </li>
                            </ul>
                        </form>
                    <?php endforeach ;?>
                <?php endif; ?>

                <?php if (sizeof($officielle) == 0 && sizeof($nonOfficielle) == 0): ?>
                    Pas de langues parlées
                <?php endif; ?>

            </div>
        </div>
    </div>
    <br>

</div>

<?php
require_once 'javascripts.php';
require_once 'footer.php';
?>