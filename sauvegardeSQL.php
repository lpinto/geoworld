<?php require_once 'header.php';?>

<!-- Cette page est un formulaire pour sauvegerder ses requêtes -->
<div class="ui container">
	<div class="page-login">
	 	<div class="ui centered grid container">
		  	<div class="nine wide column">
		      	<h2>
		      		<div >
			        	<h1 class="ui center aligned header"> <i> <u>Votre requête </u> </i></h1>
			      	</div>
		      	</h2>

		      	<div class="ui fluid card">
		      		<div class="content">
			      		<form action="ajoutBase.php" method="post" class="ui form">

                            <input type="hidden" name="idUser" value="<?php echo $_SESSION['id']; ?>">
			          		<div class="field">
			          			Nom <input type="text" name="nom" placeholder="Nom de votre requête">
			          		</div>
			          		<div class="field">
			          			Requête <input type="text" name="requete" placeholder="Requête SQL">
			          		</div>
                            <div class="field">
                                Type de requête :
                                <div class="ui radio checkbox">
                                    <input type="radio"  name="type" value="publique" />
                                    <label> Publique </label>
                                </div>
                                <div class="ui radio checkbox">
                                    <input type="radio" name="type" value ="privee"/>
                                    <label> Privée </label>
                                </div>
                            </div>

		        			<button class="ui right floated button"> Sauvegarder </button>
				       </form>
				       
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
	require_once 'javascripts.php';
	require_once 'footer.php';
?>
