<?php
	require_once 'inc/manager-db.php';
	/** Si login existe alors on ajoute l'utilisateur à la base
     */
	if (isset($_POST['login'])){
		ajoutBase($_POST);
	}

	/** Sinon if requete existe on ajoute la requête à la base */
	else if (isset($_POST['requete'])){
		ajoutRequete($_POST);
	}

	/** On se redirige vers la page d'accueil */
	header('location:index.php');
	exit();
?>