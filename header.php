<?php require_once 'inc/manager-db.php'; ?>

<!DOCTYPE html>
<html>
<head>
    <!-- Icône dans la barre de titre -->
    <link rel="icon" type="image/png" href="images/earth.png" />

    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>GeoWorld</title>
    <link rel="stylesheet" type="text/css" href="semantic/semantic.css">
    <link rel="stylesheet" type="text/css" href="dist/components/icon.css">
    <link rel="stylesheet" type="text/css" href="dist/components/flag.css">
    
    
    <!-- Regles CSS définies ou redéfinies, pour cette application -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>
    <div class="ui pointing menu inverted fixed">
        <!-- Affiche notre page d'accueil -->
        <a class="item" href='index.php' id="home"> GeoWorld </a>

        <!-- Affiche le sujet du projet -->
        <a class="item" href="todo-projet.php"> ProjetPPE-SLAM </a>

        <!-- Affiche un menu déroulant des différents continent -->
        <div class="ui simple dropdown item">
            <div class="text"> Continents </div>
                <i class="dropdown icon"></i>
            <div class="menu">
                <?php
                  $continents = getAllContinents();
                  foreach ($continents as $valeurs):
                ?>

                <div class="item">
                    <div> <a href="lesContinents.php?continent=<?php echo $valeurs->Continent; ?>&page=1"><?php echo $valeurs->Continent; ?></a> </div>
                </div>   
                <?php endforeach; ?>
            </div>
        </div>

        <div class="right menu">
            <?php

                session_start();

              //On teste pour voir si nos variables de session ont bien été enregistrées
              if (isset($_SESSION['login']) && isset($_SESSION['password']) && isset($_SESSION['role'])): ?>
                <div class="ui simple dropdown item">
                    <div class="text">  <?php echo $_SESSION['login']; ?> </div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        <div class="item">
                            <?php
                                if ($_SESSION['civilite'] == "monsieur"): ?>
                                    <div> <img class="ui medium circular image" src="assets/images/avatar/tom.jpg"> </div>
                            <?php endif; ?>
                            <?php
                                if ($_SESSION['civilite'] == "madame"): ?>
                                    <div> <img class="ui medium circular image" src="assets/images/avatar/nan.jpg"> </div>
                            <?php endif; ?>
                            <?php
                            if ($_SESSION['civilite'] == "eleve"): ?>
                                <div> <img class="ui medium circular image" src="assets/images/avatar/eleve.jpg"> </div>
                            <?php endif; ?>
                            <?php
                            if ($_SESSION['civilite'] == "admin"): ?>
                                <div> <img class="ui medium circular image" src="assets/images/avatar/admin.png"> </div>
                            <?php endif; ?>
                        </div>
                        <div class="item">
                            <div> <?php echo $_SESSION['role']; ?></div>
                        </div>
                        <?php if ($_SESSION['role'] == "enseignant"):?>
                            <div class="item">
                                <div><a href="sauvegardeSQL.php"> Requêtes </a></div>
                            </div>
                        <?php endif; ?>
                        
                        <div class="item">
                            <div> <a href="logout.php"> Déconnexion </a> </div>
                        </div>
                    </div>
                </div>

            <?php endif;
            /** Si il n'y a pas de session enregistrée on se redirige vers la page de connexion */
            if (!isset($_SESSION['login']) && !isset($_SESSION['password']) && !isset($_SESSION['role'])): ?>
                <a class="ui item" href="login.php"> Login </a>
            <?php endif; ?>

            <!-- Barre de recherche -->
            <div class="ui search">
                <form action="search.php" method="GET">
                    <div class="ui icon input">
                        <input  type="search" name="search" placeholder="Search...">
                        <button class="ui icon button">
                            <i class="search link icon"></i>
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>

