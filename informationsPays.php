<?php  
    require_once 'header.php';
    require_once 'inc/manager-db.php';
    $lePays = $_GET['nom'];
    $geop = geopolitique($lePays);
    $officielle=getOfficialLanguage($lePays);
    $nonOfficielle=getUnofficialLanguage($lePays);
    $infoCapital=capital($lePays);
 /** Cette page affiche toute les informations géopolitiques du pays selectionné */
?>
    

<div class="ui container">

        <?php
        foreach ($geop as $value) {
            $code = strtolower($value->Code2);
            $lePays = $value->Name;
        }
        ?>
        <h1 ><img class="ui avatar" src="images/drapeau/<?php echo $code; ?>.png">
            <?php echo $lePays;?>  </h1>

        <div class="ui two column grid">
            <div class="column">
                <div class="ui raised segment">
                    <a class="ui red ribbon label"> <h4> Géopolitique </h4>  </a>

                    <?php 
                    if (isset($_SESSION['login']) && isset($_SESSION['password']) && isset($_SESSION['role'])): 
                        if ($_SESSION['role'] == "enseignant"):?>
                             <a class="modifier" href="update.php?nom=<?php echo $lePays; ?>"> <i class="edit icon "></i> </a>
                        <?php endif;?>
                    <?php endif; ?>

                    <?php foreach ($geop as $value): ?>
                        <ul class="ui list">
                            <li> <h4>Population : <?php echo $value->Population; ?> habitants </h4> </li>
                            <li> <h4> Espérance de vie : <?php echo $value->LifeExpectancy; ?> ans </h4> </li>
                            <li> <h4> Superficie : <?php echo $value->SurfaceArea; ?> m²</h4> </li>
                            <li> <h4> L'année de l'indépendance : <?php echo $value->IndepYear; ?> </h4> </li>
                            <li> <h4> Type de gouvernement : <?php echo $value->GovernmentForm; ?> </h4></li>
                            <li> <h4> Chef de l'Etat : <?php echo $value->HeadOfState; ?> </h4> </li>
                        </ul>
                    <?php endforeach; ?>

                </div>
            </div>
            <div class="column">
                <div class="ui raised segment">
                    <a class="ui teal right ribbon label"> <h4> Capital </h4> </a>

                   

                    <?php if (sizeof($infoCapital) != 0): ?>
                        <?php foreach ($infoCapital as $value): ?>
                            <ul class="ui list">
                                <li> <h4>Nom : <?php echo $value->Name; ?></h4> </li>
                                <li> <h4> Population : <?php echo $value->Population; ?> habitants </h4> </li>
                            </ul>
                        <?php endforeach; ?>
                    <?php endif;?>
                    <?php if (sizeof($infoCapital) == 0): ?>
                        Pas de capital
                    <?php endif;?>
                </div>
            </div>
        </div>
        <div class="ui one column grid">
            <div class="column">
                <div class="ui raised segment">
                    <a class="ui blue ribbon label">  <h4> Langues </h4> </a>

                    <?php if (sizeof($officielle) != 0):; ?>
                        <h4>Officielle :</h4>
                            <?php foreach ($officielle as $value):?>
                                    <ul class="ui list">
                                        <li><?php echo $value->Name;?> parlé par
                                            <?php echo $value->Percentage;?> % de la population
                                            <?php if (isset($_SESSION['login']) && isset($_SESSION['password']) && isset($_SESSION['role'])): 
                                                    if ($_SESSION['role'] == "enseignant"):?>
                                                         <a class="modifier" href="update.php?nom=<?php echo $lePays; ?>"> <i class="edit icon "></i> </a>
                                                    <?php endif;?>
                                                <?php endif; ?>
                                        </li>
                                    </ul>
                                <?php endforeach ;?>
                    <?php endif; ?>

                    <?php if (sizeof($nonOfficielle) != 0):; ?>
                        <h4>Non Officielle :</h4>
                        <?php foreach ($nonOfficielle as $value):?>
                            <ul class="ui list">
                                <li><?php echo $value->Name;?> parlé par
                                    <?php echo $value->Percentage;?> % de la population
                                    <?php if (isset($_SESSION['login']) && isset($_SESSION['password']) && isset($_SESSION['role'])): 
                                            if ($_SESSION['role'] == "enseignant"):?>
                                                 <a class="modifier" href="update.php?nom=<?php echo $lePays; ?>"> <i class="edit icon "></i> </a>
                                            <?php endif;?>
                                        <?php endif; ?>
                                </li>
                            </ul>
                        <?php endforeach ;?>
                    <?php endif; ?>

                    <?php if (sizeof($officielle) == 0 && sizeof($nonOfficielle) == 0): ?>
                        Pas de langues parlées
                    <?php endif; ?>

                </div>
            </div>
        </div>
</div>

<?php
    require_once 'javascripts.php';
    require_once 'footer.php';
?>
