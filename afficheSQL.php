<?php
    require_once 'header.php';
    require_once 'inc/manager-db.php';
    $result = afficheSQL($_POST);
?>

<!-- Cette page sert à afficher dans un tableau les résultats des requêtes enregistrées
-->
<div class = "afficheSQL">
        <h1 class="ui center aligned header"> <i> <u> Résultats </u></i> </h1>
        <br>
        <table class = "ui celled table">
            <?php if (count($result) >= 1):
                foreach ($result as $nom => $value): ?>
                <thead>
                    <tr class="center aligned">
                        <?php foreach ($result[$nom] as $colonne => $valeur):?>
                                    <th>
                                        <?php echo $colonne; ?>
                                    </th>
                                    <?php endforeach; ?>
                    </tr>
                    <?php break; ?>
                <?php endforeach;?>
            <?php endif; ?>
                </thead>

                <tbody>
                <?php foreach ($result as $nom => $value):?>
                    <tr class="center aligned">
                        <?php foreach ($result[$nom] as $info): ?>
                        <td>
                            <?php echo $info; ?>
                        </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>



        </table>
</div>


<?php
require_once 'javascripts.php';
require_once 'footer.php';
?>