<?php
require_once 'connect-db.php';


/** Obtenir le nombre de pages pour les pays d'un continent donné
 * @param $continent le nom d'un continent
 * @return le nombre de page
 */
function nbPage($continent){
    global $pdo;

    $sql = 'SELECT COUNT(id) as nbPays FROM country WHERE  continent = :continent;';
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':continent', $continent, PDO::PARAM_STR);
    $prep->execute();
    $data = $prep->fetch();
    $nbPays = $data->nbPays;
    $nbPage = ceil($nbPays / 11);
    return $nbPage;
}

/** Obtenir la liste de 11 pays référencés d'un continent donné
 * par page
 */
function getCountriesByContinent($continent, $parametre)
{
    // pour utiliser la variable globale dans la fonction
    global $pdo;

    if (isset($parametre['page'])){
        $pageAct = $parametre['page'];
    }
    else{
        $pageAct = 1;
    }
    $parPage = 11;

    $query = 'SELECT * FROM country WHERE continent = :continent LIMIT :debut,:limit;';
    $prep = $pdo->prepare($query);
    $prep->bindValue(':continent', $continent, PDO::PARAM_STR);
    $prep->bindValue(':debut', (($pageAct-1)*$parPage),PDO::PARAM_INT);
    $prep->bindValue(':limit', $parPage, PDO::PARAM_INT);
    $prep->execute();
    return $prep->fetchAll();
}

/** Obtenir la liste des pays
 */
function getAllCountries()
{
    global $pdo;
    $query = 'SELECT * FROM Country;';
    return $pdo->query($query)->fetchAll();
}

/** Obtenir la liste des pays par continent
 */
function getAllContinents()
{
    global $pdo;
    $sql = 'SELECT Continent FROM Country GROUP BY Continent';
    return $pdo->query($sql)->fetchAll();
}

/** Obtenir la liste des informations géopolitiques
 * d'un pays
 */
function geopolitique($country)
{
    global $pdo;
    $query = 'SELECT * FROM Country WHERE Name = "'.$country.'"';
    return $pdo->query($query)->fetchAll();
}

/** Obtenir la capital du pays
 */
function capital($country)
{
    global $pdo;
    $sql = "SELECT city.Name, city.Population, city.id FROM city, country WHERE city.id = country.Capital AND country.Name = '".$country."'";
    return $pdo->query($sql)->fetchAll();
}

/** Obtenir la liste des langues officielle
 * d'un pays
 */
function getOfficialLanguage($country)
{
    global $pdo;
    $sql="SELECT language.Name,Percentage, idLanguage, IsOfficial FROM language, countrylanguage,country WHERE language.id = countrylanguage.idLanguage
   And countrylanguage.idCountry = country.id AND IsOfficial='T' AND country.Name ='".$country."'";
    return $pdo->query($sql)->fetchAll();
}

/** Obtenir la liste des langues non officielle
 * d'un pays
 */
function getUnofficialLanguage($country)
{
    global $pdo;
    $sql="SELECT language.Name,Percentage, idLanguage, IsOfficial FROM language, countrylanguage,country WHERE language.id = countrylanguage.idLanguage
   And countrylanguage.idCountry = country.id AND IsOfficial='F' AND country.Name ='".$country."'";
    return $pdo->query($sql)->fetchAll();
}

/** Obtenir la liste des informations
 * sur un utilisateur
 */
function getAuthentification($login, $pwd){
    global $pdo;

    $sql ="SELECT * FROM users where login=:login and password=:pwd";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':login', $login);
    $prep->bindValue(':pwd', $pwd);
    $prep->execute();

    if($prep->rowCount() == 1){
        $result = $prep->fetch();
        return $result;
    }
    else
        return false;
}

/** Obtenir la liste des utilisateurs
 */
function getAllUsers()
{
    global $pdo;
    $query = 'SELECT * FROM users;';
    return $pdo->query($query)->fetchAll();
}

/** Supprimer un utilisateur de la base
 */
function deleteUser($id)
{
    global $pdo;
    $sql = "DELETE FROM users WHERE id = :id";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':id', $id);
    $prep->execute();

}

/** Supprimer une requête de la base
 */
function deleteRequete($id, $idUser)
{
    global $pdo;
    $sql = "DELETE FROM requetes WHERE idSQL = :id";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':id', $id);
    $prep->execute();

    $sql = "DELETE FROM createurs WHERE idSQl = :id AND idUser = :idUser";

    $prep = $pdo->prepare($sql);
    $prep->bindValue(':id', $id);
    $prep->bindValue(':idUser', $idUser);

    $prep->execute();
    echo $id." ".$idUser;

}

/** Ajouter un utilisateur à la base de donnée
 */
function ajoutBase($parametre)
{
    global $pdo;
    $sql = "INSERT INTO users (civilite, nom, prenom, login, password, role) VALUES (:civilite, :nom, :prenom, :login, :password, 'enseignant')";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':civilite', $parametre['civilite']);
    $prep->bindValue(':nom', $parametre['nom']);
    $prep->bindValue(':prenom', $parametre['prenom']);
    $prep->bindValue(':login', $parametre['login']);
    $prep->bindValue(':password', $parametre['password']);
    $prep->execute();
}

/** Pour ajouter une requête à la base de donnée
 */
function ajoutRequete($parametre)
{
    global $pdo;

    $sql = "INSERT INTO requetes (nomSQL, requete, type) VALUES (:nom, :requete, :type)";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':nom', $parametre['nom']);
    $prep->bindValue(':requete', $parametre['requete']);
    $prep->bindValue(':type', $parametre['type']);
    $prep->execute();

    $sql = "SELECT idSQL FROM requetes WHERE nomSQL = :nom AND requete = :requete";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':nom', $parametre['nom']);
    $prep->bindValue(':requete', $parametre['requete']);
    $prep->execute();
    $result = $prep->fetch();

    foreach ($result as $value){
        $idSQL = $value;
    }

    $sql = "INSERT INTO createurs VALUES ($idSQL, :idUser)";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':idUser', $parametre['idUser'], PDO::PARAM_INT);
    $prep->execute();
}

/** Modifier les informations d'un pays
 */
function updateCountry($parametre)
{
    global $pdo;
print_r($parametre);
    $sql = "UPDATE country SET Name = :Name, SurfaceArea = :SurfaceArea, IndepYear = :IndepYear, Population = :Population, LifeExpectancy = :LifeExpectancy,GovernmentForm = :GovernmentForm,HeadOfState = :HeadOfState WHERE id = :id";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':Name', $parametre['NameCountry']);
    $prep->bindValue(':SurfaceArea', $parametre['SurfaceArea']);
    $prep->bindValue(':IndepYear', $parametre['IndepYear'], PDO::PARAM_INT);
    $prep->bindValue(':Population', $parametre['PopulationCountry'],PDO::PARAM_INT);
    $prep->bindValue(':LifeExpectancy', $parametre['LifeExpectancy']);
    $prep->bindValue(':GovernmentForm', $parametre['GovernmentForm']);
    $prep->bindValue(':HeadOfState', $parametre['HeadOfState']);
    $prep->bindValue(':id', $parametre['idCountry'], PDO::PARAM_INT);
    $prep->execute();

    $sql = "UPDATE city SET Name = :NameCity, Population = :PopulationCity WHERE id = :idCity";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':NameCity', $parametre['NameCity']);
    $prep->bindValue(':PopulationCity', $parametre['PopulationCity'], PDO::PARAM_INT);
    $prep->bindValue(':idCity', $parametre['idCity'], PDO::PARAM_INT);
    $prep->execute();

}

/** Modifier les langues dans la base de donnée
 */
function updateLanguage($parametre)
{
    global $pdo;

    $sql="UPDATE language SET Name = :Name WHERE id = :idLanguage ";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':Name', $parametre['NameLanguage']);
    $prep->bindValue(':idLanguage', $parametre['idLanguage'], PDO::PARAM_INT);
    $prep->execute();

    $sql="UPDATE countrylanguage SET Percentage = :Percentage WHERE idLanguage= :idLanguage AND IsOfficial = :IsOfficial";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':Percentage', $parametre['Percentage']);
    $prep->bindValue(':idLanguage', $parametre['idLanguage']);
    $prep->bindValue(':IsOfficial', $parametre['IsOfficial']);
    $prep->execute();
}

/** Obtenir la liste des recherches
 */
function getSearch($parametre)
{
    global $pdo;

    $sql = "SELECT * FROM country WHERE Name LIKE '".$_GET['search']."%'";
    return $pdo->query($sql)->fetchAll();
}

/** Obtenir la liste des requêtes pour l'utilisateur choisi
 */
function getAllRequetes($id)
{
    global $pdo;

    $sql = "SELECT * FROM requetes, createurs, users WHERE requetes.idSQL = createurs.idSQL AND users.id = createurs.idUser AND idUser = :id";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(':id', $id, PDO::PARAM_INT);
    $prep->execute();
    return $prep->fetchAll();
}

/** Obtenir la liste des requêtes publique
 */
function getAllRequetesPub()
{
    global $pdo;

    $sql = "SELECT * FROM requetes WHERE type = 'publique'";
    return $pdo->query($sql)->fetchAll();
}

/** Obtenir le résultat d'une requête enregistrée
 */
function afficheSQL($parametre)
{
    global $pdo;

    $sql = $parametre['requete'];
    return $pdo->query($sql)->fetchAll();
}

?>