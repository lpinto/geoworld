<?php require_once 'header.php'; ?>

<!-- Cette page indique un message d'erreur lorsqu'on se trompe de login ou de mot de passe -->

<div class="ui container">
	<div class="page-login">
	 	<div class="ui centered grid container">
		  	<div class="nine wide column">
		      	<h2>
                    <div class="ui negative message">
                        <i class="close icon"></i>
                        <div class="header"> Erreur! </div>
                        <p>Login et/ou Mot de Passe incorrect</p>
                    </div>
		      	</h2>	
		  
		      	<div class="ui fluid card">
		      		<div class="content">
			      		<form action="authentification.php" method="post" class="ui form">
			          		<div class="field">
			          			Login <input type="text" name="login" placeholder="Login">
			          		</div>
			          		<div class="field">
			          			Password <input type="password" name="password" placeholder="Password">
			          		</div>
		          	
		        			<button class="ui primary button" type="submit">
					          	<i class="unlock icon"></i>
					          	Connexion
				         	</button>
				       </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
	require_once 'javascripts.php';
	require_once 'footer.php';
?>
